
Copyright 2005 http://2bits.com

Description:
------------
        
This is a simple topic/answer module.

An user with the correct permission, usually an admin or a moderator,
posts topics on a web site. These are normally questions.
The user creating the node determines how many answers each user can
enter, and how many days the topic will remain open.

Users with the appropriate permissions then answer the topic. They
can also edit their answers, as long as the topic is still open.

After the defined number of days have passed, the topic and answers are 
formatted in an email and sent to the user who posted it, with the
profile fields of those who answered it.

The email is formatted to 72 columns, and the HTML tags are stripped.

Topics can be closed by the admin any time, before the specified number
of days have passed.

Although the current implementation is specific to the requirements of
one client (they wanted the results collated and emailed), I am releasing
this module because it can be a building block for more functionality.
For example, the answers can be "graded/rated" online, and the best 
answers published on the topic after it is closed. Many other variations
are possible.

Sponsored By:

http://humourauthority.com

Installation:
-------------

- Copy the topic.module file to the modules directory.

- Create the database table by running the topic.mysql script from
  phpmyadmin or mysql command line.

- Enable the module in Admin > Modules

- Grant permissions for creating topics, and answering topics as
  needed. Only registered users can answer topics.

- The following profile fields must be configured so that they are
  included in the email:

    profile_last_name
    profile_first_name
    profile_city
    profile_state

- In order for topics to be closed and sent by email, cron must be
  configured.

Author
------

Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
